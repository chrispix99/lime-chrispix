package com.trashlabs.lime;

import java.util.ArrayList;

import com.trashlabs.lime.annotations.OneToOne;
import com.trashlabs.lime.sql.Select;

class SelectBuilder {
	
	private Select select = new Select(); 
	private ArrayList<String> tables = new ArrayList<String>();
	
	public static Select build(ClassInfo info) {
		SelectBuilder builder = new SelectBuilder(); 
		return builder.buildSelect(info);
	}
	
	public Select buildSelect(ClassInfo info) {
		select.from(info.getTableName());
		select.addColumns(info.getBasicFieldNames());
		findChildren("", info);
		return select;
	}

	private void findChildren(String prefix, ClassInfo info) {
		for(FieldInfo field : info.getFields()) {
			if(field.getClassification() != FieldInfo.CLASSIFICATION_COMPLEX) continue; 
			if(!field.isAnnotationPresent(OneToOne.class)) continue; 
			OneToOne anno = field.getAnnotation(OneToOne.class);
			ClassInfo childInfo = Schema.getInfo(anno.child());
			String parentKey = (prefix == "") ? info.getTableName() + "." + anno.parentKey() : prefix + "." + anno.parentKey();
			addChild(prefix + field.getName(), childInfo, parentKey, anno.childKey());
		}
	}

	private void addChild(String prefix, ClassInfo child, String parentKey, String childKey) {
		select.addColumns(child.getBasicFieldNames(prefix));
		String table = child.getTableName();
		String expression = parentKey + "=" + prefix + "." + childKey; 
		select.leftJoin(table + " AS " + prefix, expression);
		if(tables.contains(table)) return; 
		tables.add(table);
		findChildren(prefix, child);
	}

}
