package com.trashlabs.lime;

import java.util.AbstractList;

import android.database.Cursor;

/**
 * The CursorDataList manages a cursor and behaves like a List. 
 * 
 * @param <T>
 */
public class DataList<T extends DataEntity> extends AbstractList<T> {
	
	protected Cursor cursor; 
	protected ObjectReader<T> reader; 
	
	protected DataList() {}
	
	/**
	 * Create a new DataList from a type and a cursor. 
	 * @param type
	 * @param cursor
	 * @return
	 */
	public static <T extends DataEntity> DataList<T> create(Class<?> type, Cursor cursor) {
		ClassInfo info = Schema.getInfo(type);
		ObjectReader<T> reader = info.getReader(cursor);
		return new DataList<T>(cursor, reader);
	}
	
	/**
	 * Construct a new DataList using cursor and a type. This list will manage the 
	 * cursor like a type.
	 * 
	 * @param cursor
	 * @param type
	 */
	public DataList(Cursor cursor, ObjectReader<T> reader) {
		this.cursor = cursor;
		this.reader = reader; 
	}

	/* (non-Javadoc)
	 * @see java.util.AbstractList#get(int)
	 */
	@Override
	public T get(int location) {
		return getItem(location);
	}

	/* (non-Javadoc)
	 * @see java.util.AbstractCollection#size()
	 */
	@Override
	public int size() {
		return cursor.getCount();
	}
	
	/**
	 * Private method to move the cursor to a location and map the values into 
	 * an object for that position. 
	 * 
	 * @param location
	 * @return
	 */
	private T getItem(int location) {
		cursor.moveToPosition(location);
		T item;
		try {
			item = reader.read(cursor);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return item;
	}
	
	/**
	 * Close the data list. This closes the backing cursor for this list.
	 */
	public void close() {
		cursor.close();
	}
}
