package com.trashlabs.lime.annotations;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * One to many relation
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface ManyToMany {
	Class<?> child();
	String parentKey();
	String childKey();
	String joinTable(); 
	String joinParentKey(); 
	String joinChildKey(); 
}