package com.trashlabs.lime.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * One to many relation
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface OneToMany {
	Class<?> child();
	String parentKey();
	String childKey();
}