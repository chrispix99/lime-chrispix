package com.trashlabs.lime.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotation to determine the name of a column in the database for reading 
 * and writing. 
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Table {
	String value(); 
}