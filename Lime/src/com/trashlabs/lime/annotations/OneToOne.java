package com.trashlabs.lime.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * ForeignKey annotation affects xml parsing and database storage.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface OneToOne {
	Class<?> child();
	String parentKey();
	String childKey();
}