package com.trashlabs.lime.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Annotation to mark a field database generated.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface GeneratedValue { }
