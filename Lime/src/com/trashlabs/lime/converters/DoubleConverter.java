package com.trashlabs.lime.converters;

import java.lang.reflect.Field;

import android.content.ContentValues;
import android.database.Cursor;

public class DoubleConverter extends TConverter {

	public DoubleConverter(Field field) {
		super(field);
	}

	@Override
	public void toObj(Cursor cursor, int columnIndex, Object item)
			throws IllegalArgumentException, IllegalAccessException,
			InstantiationException {
		field.set(item, cursor.getDouble(columnIndex));
	}

	@Override
	public void frObj(ContentValues values, String columnName, Object item)
			throws IllegalArgumentException, IllegalAccessException,
			InstantiationException {
		values.put(columnName, field.getDouble(item));
	}

}
