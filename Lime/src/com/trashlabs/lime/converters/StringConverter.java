package com.trashlabs.lime.converters;

import java.lang.reflect.Field;


import android.content.ContentValues;
import android.database.Cursor;

public class StringConverter extends TConverter {

	public StringConverter(Field field) {
		super(field);
	}

	@Override
	public void toObj(Cursor cursor, int columnIndex, Object item)
			throws IllegalArgumentException, IllegalAccessException,
			InstantiationException {
		field.set(item, cursor.getString(columnIndex));
		
	}

	@Override
	public void frObj(ContentValues values, String columnName, Object item)
			throws IllegalArgumentException, IllegalAccessException,
			InstantiationException {
		values.put(columnName, (String) field.get(item));
		
	}

}
