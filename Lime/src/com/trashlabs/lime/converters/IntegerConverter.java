package com.trashlabs.lime.converters;

import java.lang.reflect.Field;


import android.content.ContentValues;
import android.database.Cursor;

public class IntegerConverter extends TConverter {

	public IntegerConverter(Field field) {
		super(field);
	}

	@Override
	public void toObj(Cursor cursor, int columnIndex, Object item)
			throws IllegalArgumentException, IllegalAccessException,
			InstantiationException {
		field.setInt(item, cursor.getInt(columnIndex));
		
	}

	@Override
	public void frObj(ContentValues values, String columnName, Object item)
			throws IllegalArgumentException, IllegalAccessException,
			InstantiationException {
		values.put(columnName, field.getInt(item));
		
	}

}
