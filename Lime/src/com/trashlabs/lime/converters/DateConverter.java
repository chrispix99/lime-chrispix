package com.trashlabs.lime.converters;

import java.lang.reflect.Field;
import java.util.Date;

import android.content.ContentValues;
import android.database.Cursor;

public class DateConverter extends TConverter {

	public DateConverter(Field field) {
		super(field);
	}

	@Override
	public void toObj(Cursor cursor, int columnIndex, Object item)
			throws IllegalArgumentException, IllegalAccessException,
			InstantiationException {
		if(!cursor.isNull(columnIndex))
			field.set(item, new Date(cursor.getLong(columnIndex)));
	}

	@Override
	public void frObj(ContentValues values, String columnName, Object item)
			throws IllegalArgumentException, IllegalAccessException,
			InstantiationException {
		Date d = (Date)field.get(item);
		if(d != null)
			values.put(columnName, d.getTime());
	}

}
