package com.trashlabs.lime.test;

import com.trashlabs.lime.DataEntity;
import com.trashlabs.lime.annotations.Column;

public class Contact extends DataEntity {
	
	@Column("contact_id")
	public long contactId;
	
	@Column("display_name")
	public String displayName; 

}
